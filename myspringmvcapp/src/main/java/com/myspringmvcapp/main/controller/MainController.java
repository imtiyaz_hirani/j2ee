package com.myspringmvcapp.main.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myspringmvcapp.main.model.Customer;
 
@Controller
public class MainController {
	static {
		System.out.println("MainController called.....");
	}
	@Autowired
	HttpServletRequest request;
	@RequestMapping("/")
	public String showHome(Model model,Customer customer) {
		model.addAttribute("customer", customer);
		return "index";
	}
	@RequestMapping("/contact")
	public String showContact() {
		return "contact";
	}
	@RequestMapping("/process")
	public String process() {  //DI
		String name=request.getParameter("name");
		request.setAttribute("name", name);
		return "process";
	}
	
	@RequestMapping("/process-sf")
	public String processSF(@Valid @ModelAttribute Customer customer,
			BindingResult result) {  //DI
		 if(result.hasErrors()) {
			 System.out.println(result.getAllErrors());
			 return "index";
		 }
			 
		System.out.println(customer.getName() + "--" + customer.getEmail());
		return "process";
	}
}
