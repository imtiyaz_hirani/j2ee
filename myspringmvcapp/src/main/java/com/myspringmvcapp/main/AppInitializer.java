package com.myspringmvcapp.main;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	static {
		System.out.println("Initializer called.....");
	}
	@Override
	protected Class<?>[] getRootConfigClasses() {
		//System.out.println("getRootConfigClasses called.....");
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		Class<?>[] arry = new Class[] {ServletConfig.class};
		return arry;
	}

	@Override
	protected String[] getServletMappings() {
		//System.out.println("getServletMappings called.....");
		String[] sarry = new String[] {"/"};
		return sarry;
	}

}
