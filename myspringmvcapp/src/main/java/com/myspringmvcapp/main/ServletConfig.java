package com.myspringmvcapp.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
@Configuration
@ComponentScan(basePackages = {"com.myspringmvcapp.main.controller"})
public class ServletConfig implements WebMvcConfigurer {
	static {
		System.out.println("ServletConfig called.....");
	}
	
	@Bean
	public ViewResolver viewResolver() {
		System.out.println("bean registered....");
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("WEB-INF/jsps/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
//			/WEB-INF/jsps/contact.jsp
//			/WEB-INF/jsps/index.jsp

//prefix: /WEB-INF/jsps/
//suffix: .jsp
