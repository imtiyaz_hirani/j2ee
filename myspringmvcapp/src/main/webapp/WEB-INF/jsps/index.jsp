<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>hello Spring</h1>
<form method="get" action="<%=request.getContextPath() %>/process">
<label>Name: </label>
<input type="text" name="name">
<br />
<input type="submit" value="process">
</form>

<hr>

<sf:form method="get" action="${pageContext.request.contextPath }/process-sf" 
modelAttribute="customer">
<label>Name: </label>
<sf:input type="text" path="name"/>
<br />
<label>Email: </label>
<sf:input type="text" path="email" />
<br />
<input type="submit" value="process" />
</sf:form>

</body>
</html>