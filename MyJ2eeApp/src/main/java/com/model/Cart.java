package com.model;

public class Cart extends Product{
	private int id;
	private String username;
	private int quantity;
	private int productId;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", username=" + username + ", quantity=" + quantity + ", getTitle()=" + getTitle()
				+ ", getCategory()=" + getCategory() + ", getPrice()=" + getPrice() + "]";
	}
	
	

}
