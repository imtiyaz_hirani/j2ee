package com.service;

import java.util.List;

import com.model.Cart;
import com.model.Product;
import com.model.User;
import com.repository.UserRepository;

public class UserService {

	private UserRepository userRepository = new UserRepository();
	
	public void insertUser(User user) {
		userRepository.insertUser(user);
		
	}

	public boolean doLogin(String username, String password) {
		 
		return userRepository.doLogin(username,password);
	}

	public List<Product> fetchAllProducts() {
		 
		return userRepository.fetchAllProducts();
	}

	public void addToCart(int pid, Object attribute) {
		String username = (String)attribute; 
		//System.out.println(pid + "--" + username);
		userRepository.addToCart(pid,username);
	}

	public int getCount(String username) {
		
		return userRepository.getCount(username);
	}

	public List<Cart> fetchAllCartItems(String username) {
		 
		return userRepository.fetchAllCartItems(username);
	}

	public void deleteItem(int id) {
		userRepository.deleteItem(id);
	}

	public void updateCart(int id, int quantity) {
		 
		userRepository.updateCart(id,quantity);
	}

}
