package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.Cart;
import com.model.Product;
import com.model.User;
import com.service.UserService;

public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private UserService userService = new UserService();
	HttpSession session;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page=request.getParameter("page");
		session =  request.getSession();
		if(page == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("contact")) {
			request.getRequestDispatcher("contact.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("all_products")) {
			//pass a variable message to jsp file 
			request.setAttribute("msg", "hi from controller!!!");
			request.getRequestDispatcher("products.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("sign_up")) {
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("login")) {
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("cart")) {
			//fetch all entries from cart table and give count and list to cart
			String username= (String)session.getAttribute("username");
			int count = userService.getCount(username);
			request.setAttribute("count", count);
			
			List<Cart> listCart = userService.fetchAllCartItems(username);
			request.setAttribute("list_cart", listCart);
			request.getRequestDispatcher("cart.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("delete_item")) {
			int id = Integer.parseInt(request.getParameter("id"));
			userService.deleteItem(id);
			String username= (String)session.getAttribute("username");
			List<Cart> listCart = userService.fetchAllCartItems(username);
			request.setAttribute("list_cart", listCart);
			int count = userService.getCount(username);
			request.setAttribute("count", count);
			request.getRequestDispatcher("cart.jsp").forward(request, response);
		}
		
		if(page.equals("add_to_cart")) {
			int pid=Integer.parseInt(request.getParameter("pid"));
			userService.addToCart(pid,session.getAttribute("username") );
			List<Product> list = userService.fetchAllProducts();
			request.setAttribute("list_products", list);
			String username= (String)session.getAttribute("username");
			int count = userService.getCount(username);
			System.out.println(count);
			request.setAttribute("count", count);
			request.getRequestDispatcher("customer_dashboard.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("update_quantity")) {
			int id=Integer.parseInt(request.getParameter("id"));
			int quantity=Integer.parseInt(request.getParameter("quantity"));
			userService.updateCart(id,quantity);
			String username= (String)session.getAttribute("username");
			List<Cart> listCart = userService.fetchAllCartItems(username);
			request.setAttribute("list_cart", listCart);
			int count = userService.getCount(username);
			request.setAttribute("count", count);
			request.getRequestDispatcher("cart.jsp").forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session =  request.getSession();
		String page=request.getParameter("page"); //sign_up_form
		if(page.equalsIgnoreCase("sign_up_form")) {
			String name=request.getParameter("name");
			String username=request.getParameter("email");
			String password=request.getParameter("password");
			User user = new User();
			user.setName(name);
			user.setUsername(username);
			user.setPassword(password);
			userService.insertUser(user);
			request.setAttribute("msg", "Sign Up Success");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		if(page.equals("login_form")) {
			String username=request.getParameter("email");
			String password=request.getParameter("password");
			boolean status = userService.doLogin(username,password);
			if(status == true) {
				//add username to session
				session.setAttribute("username", username);
				List<Product> list = userService.fetchAllProducts();
				request.setAttribute("list_products", list);
				
				int count = userService.getCount(username);
				System.out.println(count);
				request.setAttribute("count", count);
				request.getRequestDispatcher("customer_dashboard.jsp").forward(request, response);
				return;
			}
			else {
				request.setAttribute("msg", "Invalid Credentials");
				request.getRequestDispatcher("login.jsp").forward(request, response);
				return;
			}
			
		}
	}

}







