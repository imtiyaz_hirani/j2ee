package com.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Cart;
import com.model.Product;
import com.model.User;

public class UserRepository {

	private String url="jdbc:mysql://localhost:3306/jdbcapp";
	private String userdb="root";
	private String passdb="";
	private String driver="com.mysql.cj.jdbc.Driver";
	Connection con;
	
	public void dbConnect() {
		/* Load the driver */
		try {
			Class.forName(driver);
			//System.out.println("driver loaded...");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		 
		/* Establish the Connection  */
		try {
			con=DriverManager.getConnection(url, userdb, passdb);
			//System.out.println("conn established..");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void dbClose() {
		try {
			con.close();
			//System.out.println("db closed..");
		} catch (SQLException e) {
 			e.printStackTrace();
		}
	}
	
	public void insertUser(User user) {
		 dbConnect();
		 String sql="insert into users(name,username,password) values (?,?,?)";
		 try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getUsername());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.executeUpdate();
			
		 } catch (SQLException e) {
			 e.printStackTrace();
		}
		 dbClose();
		
	}

	public boolean doLogin(String username, String password) {
		boolean status = false; 
		 dbConnect();
		 String sql="select * from users where username=? and password=?";
		 try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);
			ResultSet rst = preparedStatement.executeQuery();
			if(rst.next()) {
				status = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 dbClose();
		return status;
	}

	public List<Product> fetchAllProducts() {
		 dbConnect();
		 List<Product> list = new ArrayList<>();
		 String sql="select * from product";
		 try {
				PreparedStatement preparedStatement = con.prepareStatement(sql);
				ResultSet rst = preparedStatement.executeQuery();
				while(rst.next()) {
					Product product = new Product();
					product.setId(rst.getInt("id"));
					product.setTitle(rst.getString("title"));
					product.setShortDesc(rst.getString("short_desc"));
					product.setCategory(rst.getString("category"));
					product.setPrice(rst.getDouble("price"));
					list.add(product);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 dbClose();
		return list;
	}

	public void addToCart(int pid, String username) {
		dbConnect();
		String sql="select * from cart where product_id=? and username=?";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, pid);
			preparedStatement.setString(2, username);
			ResultSet rst = preparedStatement.executeQuery();
			if(rst.next() == true) {
				//if true, record already exists in DB and quantity needs to be updated 
				updateRecordInCart(rst.getInt("id"),rst.getInt("quantity")+1);
			}
			else {
				//if false, record needs to inserted
				insertRecordInCart(pid,username,1);
			}
		} catch (SQLException e) {
			 
			e.printStackTrace();
		}

		dbClose();
		
	}

	public void updateRecordInCart(int cartId, int quantity) {
		String sql="update cart SET quantity=? where id=?";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, quantity);
			preparedStatement.setInt(2, cartId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void insertRecordInCart(int pid, String username, int quantity) {
		String sql="insert into cart(product_id,username,quantity) values (?,?,?)";
		 try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, pid);
			preparedStatement.setString(2, username);
			preparedStatement.setInt(3, quantity);
			preparedStatement.executeUpdate();		
		 } catch (SQLException e) {
			 e.printStackTrace();
		}	
	}

	public int getCount(String username) {
		int count = 0; 
		 dbConnect();
		 String sql="select SUM(quantity) as cnt from cart where username=?";
		 try {
				PreparedStatement preparedStatement = con.prepareStatement(sql);
				preparedStatement.setString(1, username);
				ResultSet rst = preparedStatement.executeQuery();
				if(rst.next()) {
					count = rst.getInt("cnt");
				}			 
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 dbClose();
		return count;
	}

	public List<Cart> fetchAllCartItems(String username) {
		dbConnect();
		List<Cart> list = new ArrayList<>();
		String sql="SELECT c.id,p.title,p.price,p.category,c.quantity "
				+ " FROM product p join cart c ON p.id = c.product_id "
				+ " where c.username=?";
		 try {
				PreparedStatement preparedStatement = con.prepareStatement(sql);
				preparedStatement.setString(1, username);
				ResultSet rst = preparedStatement.executeQuery();
				while(rst.next()) {
					Cart cart = new Cart();
					cart.setId(rst.getInt("id"));
					cart.setTitle(rst.getString("title"));
					cart.setCategory(rst.getString("category"));
					cart.setPrice(rst.getDouble("price"));
					cart.setQuantity(rst.getInt("quantity"));
					list.add(cart);
				}			 
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		dbClose();
		return list;
	}

	public void deleteItem(int id) {
		dbConnect();
		String sql="delete from cart where id=?";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbClose();
	}

	public void updateCart(int id, int quantity) {
		 dbConnect();
		 updateRecordInCart(id,quantity);
		 dbClose();
		
	}

}











