<%@page import="com.model.Cart"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
.tbl{
	width: 100%;
	padding: 10px;
	margin: 10px;
}
</style>
<script>
function showUpdate(id){
	 document.getElementById('update'+id).style.display="block"
} 
</script>
<body>
<jsp:include page="header.jsp"></jsp:include>
<% List<Cart> list = (List<Cart>)request.getAttribute("list_cart");  %>
<table class="tbl">
					<thead>
						<tr style="text-align: left;"> 
							<th> Sr. No</th>
							<th> Product Title</th>
							<th> Price </th>
							<th> Quantity</th>
							<th> Total</th>
							<th></th>
						</tr>	
					</thead>			 
					<tbody>
<% 
int sr=1;
double totalAmount = 0; 
	for(Cart c : list){
		totalAmount = totalAmount + (c.getPrice()*c.getQuantity());
		%>
			<tr>
			<td><br ><%=sr++ %></td>
			<td><br ><%=c.getTitle() %></td>
			<td><br >INR. <%=c.getPrice() %></td>
			<td><br > 
			<form action="MainController" method="get">
			<input type="hidden" name="page" value="update_quantity">
			<input type="hidden" name="id" value="<%=c.getId() %>">
			<input type="number" name="quantity" value="<%=c.getQuantity() %>"
			 style="width: 40px" onKeyUp="showUpdate(<%=c.getId() %>)" >
			<span style="display: none;" id="update<%=c.getId() %>">
			<input type="submit" value="update">
			</span>
			</form>
			</td>
			<td><br >INR. <%=c.getPrice()*c.getQuantity()  %></td>
			<td><a href="MainController?page=delete_item&id=<%=c.getId() %>">delete</a></td>
			</tr>
		<% 
	}
%>
<tr>
<td> <br></td><td> </td><td> </td> 
<td colspan="2" style="text-align: center;"> <br><br>Total Amount: INR <%=totalAmount %> </td>
</tr>

</tbody>
</table>
</body>
</html>