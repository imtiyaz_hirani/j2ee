<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Login</h1>
<%=request.getAttribute("msg")!=null?request.getAttribute("msg"):"" %>

<fieldset>
<legend>Login</legend>
<form action="MainController?page=login_form" method="post">
 <label>Enter Username/Email:</label> 
 <input type="email" name="email" required="required">  
	<br /><br />
<label>Enter Password:</label> <input type="password" name="password" required="required">  
	<br /><br />
<input type="submit" value="Login">
</form>
</fieldset>
<p>Don't have an Account? <a href="MainController?page=sign_up">Sign Up</a></p>
</body>
</html>