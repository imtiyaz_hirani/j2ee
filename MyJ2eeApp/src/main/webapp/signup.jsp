<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Sign Up</h1>


	<fieldset style="width: 50%">
		<legend>Sign Up</legend>
		<form action="MainController?page=sign_up_form" method="post">
			<label>Enter Name:</label> <input type="text" name="name" required="required">  
			<br /><br />
			<label>Enter Username/Email:</label> <input type="email" name="email" required="required">  
			<br /><br />
			<label>Enter Password:</label> <input type="password" name="password" required="required">  
			<br /><br />
			<input type="submit" value="Sign Up">
		</form>
	</fieldset>
	<br />
	<p>Already have an acount? <a href="MainController?page=login">Login</a>
</body>
</html>