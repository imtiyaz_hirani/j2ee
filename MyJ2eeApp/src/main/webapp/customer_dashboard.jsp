<%@page import="com.model.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.product_card{
	border: 1px solid #000000;
	width: 28%;
	float: left;
	padding: 10px;
	margin: 10px;
	box-shadow: grey 5px 7px;
}

a {
	text-decoration: none; 
		
}
</style> 
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<%
 List<Product> list =(List<Product>) 
 	request.getAttribute("list_products");

for(Product p : list){
%>
	<div class="product_card">
		<h3><%=p.getTitle() %></h3>
		<p><%=p.getShortDesc() %> </p>
		<span>Price: INR.<%=p.getPrice() %></span>
		<br />
		<span>Category: <%=p.getCategory() %></span> 
		<br /><br />
		<a href="MainController?page=add_to_cart&pid=<%=p.getId() %>">Add to Cart</a>
	</div>
	
<%
}
%>
</body>
</html>