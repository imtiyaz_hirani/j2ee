package com.springboot.bank.main.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.bank.main.model.User;
import com.springboot.bank.main.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/auth/login")
	public User login(Principal principal) {
		String username = principal.getName();
		User user  = (User) userService.loadUserByUsername(username);
		user.setPassword("");
		return user;
	}
}
