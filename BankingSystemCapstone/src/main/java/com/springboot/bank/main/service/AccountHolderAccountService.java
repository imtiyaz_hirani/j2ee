package com.springboot.bank.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.springboot.bank.main.enums.AccountType;
import com.springboot.bank.main.model.AccountHolder;
import com.springboot.bank.main.model.AccountHolderAccount;
import com.springboot.bank.main.repository.AccountHolderAccountRepository;
 
@Service
public class AccountHolderAccountService {

	@Autowired
	private AccountHolderAccountRepository accountHolderAccountRepository;
	
	public AccountHolderAccount insertAHA(AccountHolderAccount aha) { 
		return accountHolderAccountRepository.save(aha);
	}

	public AccountHolderAccount getRecordByAhId(int ahId) {
		 
		return accountHolderAccountRepository.getRecordByAhId(ahId);
	}

	public AccountHolderAccount getAccountHolderByContact(String contact) {
		return accountHolderAccountRepository.getAccountHolderByContact(contact);
	}

	public List<AccountHolder> getAccountHolderByAccountTypewithPagination(AccountType type, 
			Pageable pageable) {		 
		return accountHolderAccountRepository.getAccountHolderByAccountTypewithPagination(type,pageable);
	}


}
