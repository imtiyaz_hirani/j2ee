package com.springboot.bank.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.bank.main.model.Account;
import com.springboot.bank.main.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	public Account insertAccount(Account account) {
		return accountRepository.save(account);
	}
 

}

/*
 * T save(T)
 * List<T> findAll()
 * Optional<T> findById(int)
 * void deleteById(id)
 * */
