package com.springboot.bank.main.controller;

import java.security.Principal;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.bank.main.dto.AccountDto;
import com.springboot.bank.main.enums.AccountType;
import com.springboot.bank.main.model.Account;
import com.springboot.bank.main.model.AccountHolder;
import com.springboot.bank.main.model.AccountHolderAccount;
import com.springboot.bank.main.model.Address;
import com.springboot.bank.main.model.Executive;
import com.springboot.bank.main.model.User;
import com.springboot.bank.main.service.AccountHolderAccountService;
import com.springboot.bank.main.service.AccountHolderService;
import com.springboot.bank.main.service.AccountService;
import com.springboot.bank.main.service.AddressService;
import com.springboot.bank.main.service.ExecutiveService;
import com.springboot.bank.main.service.UserService;

@RestController
public class AccountController {

	Logger logger = LoggerFactory.getLogger(""); 
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountHolderService accountHolderService;
	
	@Autowired
	private AccountHolderAccountService accountHolderAccountService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private ExecutiveService executiveService;
	/*
	 * Aim: to insert account entity
	 * Path: /account/add
	 * Param: --
	 * RequestBody: Account acccount 
	 * Response/return type: Account 
	 * */
	@PostMapping("/account/add")
	public AccountHolderAccount postAccount(@RequestBody AccountDto dto,Principal principal) {
		//logger.info(dto.getCity());
		//System.out.println(dto);
		/*
		 * 1. Address insertion
		 */
		Address address = new Address();
		address.setStreet(dto.getAddress().getStreet());
		address.setPincode(dto.getAddress().getPincode());
		address.setCity(dto.getAddress().getCity());
		address.setState(dto.getAddress().getState());
		address = addressService.insertAddress(address);
		
		/*
		 * 2. User insertion
		 */
		//check if given username exists in the DB 
		User user = userService.validateUsername(dto.getUser().getUsername());
		
		if(user == null) {
			user= new User();
			user.setUsername(dto.getUser().getUsername());
			String tempPassword="ABC123abc#";
			
			user.setPassword(passwordEncoder.encode(tempPassword));
			user.setRole("ACCOUNT_HOLDER");
			user = userService.insertUser(user);
		}
		 
		
		/*
		 * 3. AccountHolder insertion  
		 */
		AccountHolder accountHolder = new AccountHolder();
		accountHolder.setName(dto.getName());
		accountHolder.setAge(dto.getAge());
		accountHolder.setContact(dto.getContact());
		accountHolder.setAddress(address);
		accountHolder.setUser(user);
		accountHolder= accountHolderService.insertAccountHolder(accountHolder);
		/*
		 * 4. Account insertion
		 */
		Account account = new Account();
		String type=dto.getAccount().getType();
		account.setType(AccountType.valueOf(type));
		account.setBalance(AccountType.valueOf(type).getMinBalance());
		account.setRateOfInterest(AccountType.valueOf(type).getRateOfInterest());
		account = accountService.insertAccount(account);
		
		/* 
		 * 5. Attach executive to AccountHolderAccount 
		 * */
		String username = principal.getName();
		//go to db and fetch executive obj on the bases of username
		Executive executive = executiveService.fetchByUsername(username);
		/*
		 * 6. AccountHolderAccount
		 */
		AccountHolderAccount aha = new AccountHolderAccount();
		aha.setAccountHolder(accountHolder);
		aha.setAccount(account);
		
		aha.setDateOfCreation(LocalDate.now());
		aha = accountHolderAccountService.insertAHA(aha);
		aha.setExecutive(executive);
		aha = accountHolderAccountService.insertAHA(aha);
		return aha;
	}
	
	@GetMapping("/api/test")
	public String test() {
		return "test"; 
	}
	
	@PostMapping("/api/test1")
	public String test1() {
		return "test1";
	}
	
	
}
