package com.springboot.bank.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.springboot.bank.main.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Autowired
	private UserService userService; 
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
			.csrf(AbstractHttpConfigurer::disable)
	       // .cors(AbstractHttpConfigurer::disable)

			.authorizeHttpRequests(authorize -> authorize
				.requestMatchers(HttpMethod.GET, "/api/test").permitAll()	
				.requestMatchers(HttpMethod.POST, "/api/test1").authenticated()
				.requestMatchers(HttpMethod.POST, "/account/add").hasAnyAuthority("EXECUTIVE")	
				.requestMatchers(HttpMethod.POST, "/executive/add").permitAll()	
				.requestMatchers(HttpMethod.GET, "/account-holder/one/{username}").permitAll()	
				.requestMatchers(HttpMethod.GET, "/account-holder/one/contact").permitAll()	
				.requestMatchers(HttpMethod.GET, "/account-holder/type/{type}").hasAnyAuthority("EXECUTIVE")
				.requestMatchers(HttpMethod.PUT, "/account-holder/update/online").hasAnyAuthority("ACCOUNT_HOLDER")
				.requestMatchers(HttpMethod.GET, "/auth/login").authenticated()
				.anyRequest().denyAll()
			)
			.httpBasic(Customizer.withDefaults());
		return http.build();
	}
	/*
	@Bean                                                             
	public UserDetailsService userDetailsService() throws Exception {
		// ensure the passwords are encoded properly
		UserBuilder users = User.withDefaultPasswordEncoder();
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(users.username("harry@gmail.com")
				.password("1234").authorities("ACCOUNT_HOLDER").build());
		manager.createUser(users.username("albus@gmail.com")
				.password("1234").authorities("EXECUTIVE").build());
		return manager;
	}
	*/
	
	@Bean
	public AuthenticationManager authenticationManager() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userService);
		authenticationProvider.setPasswordEncoder(getEncoder());

		return new ProviderManager(authenticationProvider);
	} 
	
	@Bean
	public PasswordEncoder getEncoder() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
}
