package com.springboot.bank.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.bank.main.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>{

}
