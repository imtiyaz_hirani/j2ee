package com.springboot.bank.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.bank.main.model.AccountHolder;
import com.springboot.bank.main.repository.AccountHolderRepository;

@Service
public class AccountHolderService {

	@Autowired
	private AccountHolderRepository accountHolderRepository;

	public AccountHolder insertAccountHolder(AccountHolder accountHolder) {
		return accountHolderRepository.save(accountHolder);
	}

	public AccountHolder getAccountHolderByUsername(String username) {
		 
		return accountHolderRepository.getAccountHolderByUsername(username);
	}

}
