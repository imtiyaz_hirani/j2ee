package com.springboot.bank.main.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.bank.main.enums.AccountType;
import com.springboot.bank.main.model.AccountHolder;
import com.springboot.bank.main.model.AccountHolderAccount;

import jakarta.transaction.Transactional;

public interface AccountHolderAccountRepository extends JpaRepository<AccountHolderAccount, Integer>{

	@Query("select aha from AccountHolderAccount aha where aha.accountHolder.id=?1")
	AccountHolderAccount getRecordByAhId(int ahId);

	@Query("select aha from AccountHolderAccount aha where aha.accountHolder.contact=?1")
	AccountHolderAccount getAccountHolderByContact(String contact);

	@Query("select aha.accountHolder from AccountHolderAccount aha where aha.account.type=?1")
	List<AccountHolder> getAccountHolderByAccountTypewithPagination(AccountType type, Pageable pageable);

}
