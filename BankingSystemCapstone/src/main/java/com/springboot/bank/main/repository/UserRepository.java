package com.springboot.bank.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.bank.main.model.User;

public interface UserRepository extends  JpaRepository<User, Integer>{

	User findByUsername(String username);

}
