package com.springboot.bank.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.bank.main.model.Executive;
import com.springboot.bank.main.model.User;
import com.springboot.bank.main.service.ExecutiveService;
import com.springboot.bank.main.service.UserService;

@RestController
public class ExecutiveController {

	@Autowired
	private ExecutiveService executiveService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/executive/add")
	public Executive postExecutive(@RequestBody Executive executive) {
		
		/* Save user in DB*/
		User user = executive.getUser();
		//encode the password 
		String encodedPass = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPass);
		
		//assign the role
		user.setRole("EXECUTIVE");
		//save user
		user = userService.insertUser(user);
		
		/* Attach user to executive */
		executive.setUser(user);
		
		/*Save executive in DB */
		executive = executiveService.insertExecutive(executive);
		return executive;
	}
}
