package com.springboot.bank.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.bank.main.model.Address;
import com.springboot.bank.main.repository.AddressRepository;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;
	
	public Address insertAddress(Address address) {
		return addressRepository.save(address);
	}

}
