package com.springboot.bank.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.bank.main.model.Executive;

public interface ExecutiveRepository extends JpaRepository<Executive, Integer>{

	@Query("select e from Executive e where e.user.username=?1")
	Executive fetchByUsername(String username);

}
