package com.springboot.bank.main.enums;

public enum AccountType {
	SAVINGS(1000d,6.0d),
	CURRENT(10000d,0.0d),
	DMAT(50000d,0.0d);
	
	AccountType(double minBalance, double rateOfInterest) {
		this.minBalance = minBalance;
		this.rateOfInterest = rateOfInterest;
	}
	
	private double minBalance; 
	private double rateOfInterest;
	
	public double getMinBalance() {
		return minBalance;
	}
	public double getRateOfInterest() {
		return rateOfInterest;
	} 
	
	
}
