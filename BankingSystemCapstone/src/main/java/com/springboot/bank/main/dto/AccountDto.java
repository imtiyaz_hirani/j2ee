package com.springboot.bank.main.dto;

 
public class AccountDto {
	private String name;
	private int age;
	private String contact;
	private AddressDto address; 
	private UserDto user; 
	private AccountTypeDto account;
	
	public static class AddressDto{
		private String street;
		private String pincode;
		private String city;
		private String state;
		public String getStreet() {
			return street;
		}
		public void setStreet(String street) {
			this.street = street;
		}
		public String getPincode() {
			return pincode;
		}
		public void setPincode(String pincode) {
			this.pincode = pincode;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		@Override
		public String toString() {
			return "AddressDto [street=" + street + ", pincode=" + pincode + ", city=" + city + ", state=" + state
					+ "]";
		}
		
		
	}

	public static class UserDto{
		private String username;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		@Override
		public String toString() {
			return "UserDto [username=" + username + "]";
		}	
	}
	
	public static class AccountTypeDto{
		private String type;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return "AccountTypeDto [type=" + type + "]";
		}
		
		
	}
 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public AccountTypeDto getAccount() {
		return account;
	}

	public void setAccount(AccountTypeDto account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "AccountDto [name=" + name + ", age=" + age + ", contact=" + contact + ", address=" + address + ", user="
				+ user + ", account=" + account + "]";
	}

	
	 
	
}
