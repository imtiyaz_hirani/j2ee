package com.springboot.bank.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.bank.main.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer>{

}
