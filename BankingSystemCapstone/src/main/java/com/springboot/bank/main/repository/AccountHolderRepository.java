package com.springboot.bank.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.bank.main.model.AccountHolder;

public interface AccountHolderRepository extends JpaRepository<AccountHolder, Integer>{

	@Query("select ah from AccountHolder ah where ah.user.username=?1")
	AccountHolder getAccountHolderByUsername(String username);

}
