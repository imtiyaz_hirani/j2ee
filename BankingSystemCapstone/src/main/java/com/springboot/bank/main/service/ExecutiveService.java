package com.springboot.bank.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.bank.main.model.Executive;
import com.springboot.bank.main.repository.ExecutiveRepository;

@Service
public class ExecutiveService {

	@Autowired
	private ExecutiveRepository executiveRepository;
	
	public Executive insertExecutive(Executive executive) {
		return executiveRepository.save(executive);
	}

	public Executive fetchByUsername(String username) {
		return executiveRepository.fetchByUsername(username);
	}

}
