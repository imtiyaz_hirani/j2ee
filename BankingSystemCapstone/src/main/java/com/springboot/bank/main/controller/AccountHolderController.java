package com.springboot.bank.main.controller;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.bank.main.dto.AccountDto;
import com.springboot.bank.main.enums.AccountType;
import com.springboot.bank.main.model.AccountHolder;
import com.springboot.bank.main.model.AccountHolderAccount;
import com.springboot.bank.main.service.AccountHolderAccountService;
import com.springboot.bank.main.service.AccountHolderService;
import com.springboot.bank.main.service.AccountService;
import com.springboot.bank.main.service.AddressService;
import com.springboot.bank.main.service.ExecutiveService;
import com.springboot.bank.main.service.UserService;

@RestController
@CrossOrigin(origins = {"http://localhost:3000","http://localhost:3001" })
public class AccountHolderController {

Logger logger = LoggerFactory.getLogger(""); 
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private UserService userService;
	 
	@Autowired
	private AccountHolderService accountHolderService;
	
	@Autowired
	private AccountHolderAccountService accountHolderAccountService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private ExecutiveService executiveService;
	
	@GetMapping("/account-holder/one/{username}")
	public AccountHolderAccount getAccountHolderByUsername(@PathVariable("username") String username){
		AccountHolder accountHolder = accountHolderService.getAccountHolderByUsername(username);
		AccountHolderAccount aha= accountHolderAccountService.getRecordByAhId(accountHolder.getId());
		
		return aha;
	}
	
	@GetMapping("/account-holder/one/contact")
	public AccountHolderAccount getAccountHolderByContact(@RequestParam("contact") String contact){
		//i will go AccountHolderAccount and provide contact of AccountHolder
		AccountHolderAccount aha=accountHolderAccountService.getAccountHolderByContact(contact);
		return aha;
	}
	
	@GetMapping("/account-holder/type/{type}")
	public ResponseEntity<?> getAccountHolderByAccountTypewithPagination(
			@PathVariable("type") String type,
			@RequestParam(value="page",required = false,defaultValue = "0") Integer page,
			@RequestParam(value="size",required = false,defaultValue = "10000") Integer size
			) {
		
		AccountType typeVal  = null;
		try {
				typeVal = AccountType.valueOf(type);
				
		}
		catch(IllegalArgumentException e) {
			logger.info(e.getClass().toString());
			//System.out.println("in catch....");
			String msg="[";
			for(AccountType t :AccountType.values() ) {
				msg = msg + t.toString() + ",";
			}
			msg = msg.substring(0, msg.length() -1);
			msg = msg + "]";
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad value of Enum given: " + type + " following values are allowed "+ msg);
		}
	 
		try {
			Pageable pageable =  PageRequest.of(page, size);
			List<AccountHolder> accountHolder = accountHolderAccountService
				.getAccountHolderByAccountTypewithPagination(typeVal,pageable);
			return ResponseEntity.status(HttpStatus.OK).body(accountHolder );
		}
		catch(IllegalArgumentException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("Bad value of Page or Size given, page must be positive or zero and size must be positive and greater than zero" );

		}
	}
	
	@PutMapping("/account-holder/update/{ahId}")
	public void updateAccountHolderAddress(@RequestBody AccountDto dto,@PathVariable("ahId") int ahId,Principal principal ) {
		//go to db and fetch existing acount-holder details by ahId
		
		//update existing entries with new entries given in dto 
		
		//save the new entry object again in db 
	}
	
	@PutMapping("/account-holder/update/online")
	public AccountHolder updateAccountHolderAddress(@RequestBody AccountDto dto,Principal principal) {
		String username = principal.getName();
		//go to db and fetch existing acount-holder details by username
		AccountHolder accountHolder = accountHolderService.getAccountHolderByUsername(username);
		//update existing entries with new entries given in dto 
		accountHolder.getAddress().setCity(dto.getAddress().getCity());
		accountHolder.getAddress().setPincode(dto.getAddress().getPincode());
		
		
		//save the new entry object again in db 
		return accountHolderService.insertAccountHolder(accountHolder);
	}
}














